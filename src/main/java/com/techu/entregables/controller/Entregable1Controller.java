package com.techu.entregables.controller;

import com.techu.entregables.model.Producto;
import com.techu.entregables.model.ProductoPrecio;
import com.techu.entregables.model.Usuario;
import com.techu.entregables.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("${url.base}")
@CrossOrigin(origins="*", methods={RequestMethod.GET, RequestMethod.POST})
public class Entregable1Controller {

    @Autowired
    private ProductoService productoService;

    @GetMapping("")
    public String home(){
        return "API REST Tech U! v1.0.0";
    }

    // listar todos los productos
    @GetMapping("/productos")
    public List<Producto> getProductos() {
        return productoService.getProductos();
    }

    // adicionar nuevo producto
    @PostMapping("/productos")
    public ResponseEntity postProducto(@RequestBody Producto newProduct) {
        productoService.addProducto(newProduct);
        return new ResponseEntity<>("Producto creado correctamente.", HttpStatus.CREATED);
    }

    // Obtener producto x ID
    @GetMapping("/productos/{id}")
    public ResponseEntity getProductoById(@PathVariable long id){
        Producto pr = productoService.getProductoById(id);
        if (pr == null) { // no existe el producto
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(pr, HttpStatus.OK);  // return ResponseEntity.ok(pr);
    }

    // Actualizar producto x ID
    @PutMapping("/productos/{id}")
    public ResponseEntity putProductoById(@PathVariable long id,
                                          @RequestBody Producto productoModel){
        Producto pr = productoService.getProductoById(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productoService.updateProductoById(id, productoModel);
        return new ResponseEntity<>("Producto actualizado correctamente.", HttpStatus.OK);
    }

    // Eliminar producto x id
    @DeleteMapping("/productos/{id}")
    public ResponseEntity deleteProductoById(@PathVariable long id){
        Producto pr = productoService.getProductoById(id);
        if(pr == null) {
            return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);
        }
        productoService.removeProductoById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT); // http code: 204
    }

    // Actualización parcial 'precio'
    @PatchMapping("/productos/{id}")
    public ResponseEntity patchProductoById(@PathVariable long id,
                                            @RequestBody ProductoPrecio productoPrecio){
        Producto pr = productoService.getProductoById(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productoService.updateProductoPrecioById(id, productoPrecio.precio);
        return new ResponseEntity<>("Producto actualizado correctamente.", HttpStatus.OK);
    }

    // Colección de los usuarios del producto {id}. GET, POST.
    @GetMapping("/productos/{id}/users")
    public ResponseEntity getUsers(@PathVariable long id){
        Producto pr = productoService.getProductoById(id);
        if(pr == null){
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        if (pr.usuarios!=null) {
            return ResponseEntity.ok(pr.usuarios);
        } else {
            return new ResponseEntity<>("[]", HttpStatus.OK);
        }
    }

    // Colección de los usuarios del producto {id}. GET, POST.
    @PostMapping("/productos/{id}/users")
    public ResponseEntity getUsersPost(@PathVariable long id){
        Producto pr = productoService.getProductoById(id);
        if(pr == null){
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        if (pr.usuarios!=null) {
            return ResponseEntity.ok(pr.usuarios);
        } else {
            return new ResponseEntity<>("[]", HttpStatus.OK);
        }
    }

    // Usuario particular del producto {id_prod} GET
    @GetMapping("/productos/{id_prod}/usuarios/{id_usuario}")
    public ResponseEntity getProductoUsuarioById(@PathVariable long id_prod, @PathVariable long id_usuario){
        Producto pr = productoService.getProductoById(id_prod);
        if (pr == null) { // no existe el producto
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }else {
            Usuario us = productoService.getProductoUsuarioById(id_prod, id_usuario);
            if (us == null) { // no existe el usuario
                return new ResponseEntity<>("Usuario no encontrado.", HttpStatus.NOT_FOUND);
            }else {
                return new ResponseEntity(us, HttpStatus.OK);
            }
        }
    }

    // Usuario particular del producto {id_prod} PUT
    @PutMapping("/productos/{id_prod}/usuarios/{id_usuario}")
    public ResponseEntity putProductoUsuarioById(@PathVariable long id_prod, @PathVariable long id_usuario,
                                          @RequestBody Usuario usuario){
        Producto pr = productoService.getProductoById(id_prod);
        if (pr == null) { // no existe el producto
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }else {
            Usuario us = productoService.getProductoUsuarioById(id_prod, id_usuario);
            if (us == null) { // no existe el usuario
                return new ResponseEntity<>("Usuario no encontrado.", HttpStatus.NOT_FOUND);
            }else {
                productoService.updateProductoUsuarioById(id_prod, id_usuario, usuario);
                return new ResponseEntity<>("Producto actualizado correctamente.", HttpStatus.OK);
            }
        }
    }

    // Usuario particular del producto {id_prod} DELETE
    @DeleteMapping("/productos/{id_prod}/usuarios/{id_usuario}")
    public ResponseEntity deleteProductoUsuarioById(@PathVariable long id_prod, @PathVariable long id_usuario){
        Producto pr = productoService.getProductoById(id_prod);
        if (pr == null) { // no existe el producto
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }else {
            Usuario us = productoService.getProductoUsuarioById(id_prod, id_usuario);
            if (us == null) { // no existe el usuario
                return new ResponseEntity<>("Usuario no encontrado.", HttpStatus.NOT_FOUND);
            }else {
                productoService.removeProductoUsuarioById(id_prod, id_usuario);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT); // http code: 204
            }
        }
    }

    // Usuario particular del producto {id_prod} PATCH
    @PatchMapping("/productos/{id_prod}/usuarios/{id_usuario}")
    public ResponseEntity patchProductoUsuarioById(@PathVariable long id_prod, @PathVariable long id_usuario,
                                            @RequestBody Usuario usuario){
        Producto pr = productoService.getProductoById(id_prod);
        if (pr == null) { // no existe el producto
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }else {
            Usuario us = productoService.getProductoUsuarioById(id_prod, id_usuario);
            if (us == null) { // no existe el usuario
                return new ResponseEntity<>("Usuario no encontrado.", HttpStatus.NOT_FOUND);
            }else {
                productoService.updateProductoUsuarioNombreById(id_prod, id_usuario, usuario.nombre);
                return new ResponseEntity<>("Producto actualizado correctamente.", HttpStatus.OK);
            }
        }
    }



//    Ejemplo para recibir una lista de parámetros en url
//    @GetMapping("/params")
//    public String getParameterList(@RequestParam(name="param") String[] paramList) {
//        String msg = "";
//        int i = 0;
//        if (paramList == null) {
//            msg = "Lista de parámetros vacía.";
//        } else {
//            for (String param : paramList) {
//                msg += "param[" + i + "] = " + param + "\n";
//                i++;
//            }
//        }
//        return msg;
//    }
}
