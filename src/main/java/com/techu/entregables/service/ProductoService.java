package com.techu.entregables.service;

import com.techu.entregables.model.Producto;
import com.techu.entregables.model.Usuario;
import org.springframework.stereotype.Component;
import java.util.ArrayList;

@Component
public class ProductoService {

    private ArrayList<Producto> productoList = new ArrayList<>();

    public ProductoService() {
        productoList.add(new Producto(1, "samsung", "tv oled 55", 2000.00));
        productoList.add(new Producto(2, "lg", "tv qled 50", 1500.00));
        productoList.add(new Producto(3, "panasonic", "tv led hd 42", 1300.00));
        productoList.add(new Producto(4, "phillips", "tv led full hd 60", 1800.50));
        productoList.add(new Producto(5, "sony", "tv led bravia 55", 1900.50));
        ArrayList<Usuario> users = new ArrayList<>();
        users.add(new Usuario(1, "ANA"));
        users.add(new Usuario(3, "PEDRO"));
        users.add(new Usuario(5, "MATEO"));
        productoList.get(1).usuarios = users;
    }

    // Listar productos
    public ArrayList<Producto> getProductos() {
        return productoList;
    }

    // Obtener producto x id
    public Producto getProductoById(long id) {
        if (getIndex(id)>=0){
            return productoList.get(getIndex(id));
        }
        return null;
    }

    // Crear nuevo producto
    public Producto addProducto(Producto nuevoProducto) {
        productoList.add(nuevoProducto);
        return nuevoProducto;
    }

    // Actualizar producto
    public Producto updateProductoById(long id, Producto newPro){
        int pos = getIndex(id);
        if (pos >= 0) {
            productoList.set(pos, newPro);
            return productoList.get(pos);
        }
        return null;
    }

    // Actualizacion parcial producto
    public Producto updateProductoPrecioById(long id, double newPrecio){
        int pos = getIndex(id);
        if (pos >= 0) {
            Producto pr = productoList.get(pos);
            pr.precio = newPrecio;
            productoList.set(pos, pr);
            return productoList.get(pos);
        }
        return null;
    }

    // Eliminar producto
    public void removeProductoById(long id) {
        int pos = getIndex(id);
        if (pos >= 0) {
            productoList.remove(pos);
        }
    }

    public int getIndex(long id) {
        int i = 0;
        while (i < productoList.size()) {
            if (productoList.get(i).id==id) {
                return(i);
            }
            i++; }
        return -1;
    }

    public int getIndex(Producto pr, long id) {
        int i = 0;
        while (i < pr.usuarios.size()) {
            if (pr.usuarios.get(i).id==id) {
                return(i);
            }
            i++; }
        return -1;
    }

    //Obtener producto usuario
    public Usuario getProductoUsuarioById(long idproducto, long idusuario) {
        if (getIndex(idproducto)>=0){
            Producto pr = productoList.get(getIndex(idproducto));
            if (getIndex(pr, idusuario)>=0){
                return pr.usuarios.get(getIndex(pr, idusuario));
            }
        }
        return null;
    }

    // Actualizar producto usuario
    public Usuario updateProductoUsuarioById(long idproducto, long idusuario, Usuario newUsr){
        int pos_p = getIndex(idproducto);
        int pos_u = -1;
        if (pos_p>=0){
            Producto pr = productoList.get(pos_p);
            pos_u = getIndex(pr, idusuario);
            if (pos_u>=0){
                pr.usuarios.set(pos_u, newUsr);
                return pr.usuarios.get(pos_u);
            }
        }
        return null;
    }
    public void removeProductoUsuarioById(long idproducto, long idusuario) {
        int pos_p = getIndex(idproducto);
        int pos_u = -1;
        if (pos_p>=0){
            Producto pr = productoList.get(pos_p);
            pos_u = getIndex(pr, idusuario);
            if (pos_u>=0){
                pr.usuarios.remove(pos_u);
            }
        }
    }
    public Usuario updateProductoUsuarioNombreById(long idproducto, long idusuario, String nombre){
        int pos_p = getIndex(idproducto);
        int pos_u = -1;
        if (pos_p>=0){
            Producto pr = productoList.get(pos_p);
            pos_u = getIndex(pr, idusuario);
            if (pos_u>=0){
                Usuario usr = pr.usuarios.get(pos_u);
                usr.nombre = nombre;
                pr.usuarios.set(pos_u, usr);
                return pr.usuarios.get(pos_u);
            }
        }
        return null;
    }
}
