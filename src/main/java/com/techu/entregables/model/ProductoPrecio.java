package com.techu.entregables.model;

public class ProductoPrecio {
    public long id;
    public double precio;

    public ProductoPrecio() {};

    public ProductoPrecio(long id, double precio) {
        this.id = id;
        this.precio = precio;
    }
}
