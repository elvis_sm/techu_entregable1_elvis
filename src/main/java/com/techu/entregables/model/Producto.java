package com.techu.entregables.model;

import java.util.List;

public class Producto {
    public long id;
    public String marca;
    public String descripcion;
    public double precio;
    public List<Usuario> usuarios;

    public Producto(){}

    public Producto(long id, String marca, String descripcion, double precio, List<Usuario> usuarios) {
        this.id = id;
        this.marca = marca;
        this.descripcion = descripcion;
        this.precio = precio;
        this.usuarios = usuarios;
    }

    public Producto(long id, String marca, String descripcion, double precio) {
        this.id = id;
        this.marca = marca;
        this.descripcion = descripcion;
        this.precio = precio;
    }
}
