package com.techu.entregables.model;

public class Usuario {
    public long id;
    public String nombre;

    public Usuario() {}

    public Usuario(long id) {
        this.id = id;
    }

    public Usuario(long id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }
}
